// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;
import "remix_tests.sol"; // this import is automatically injected by Remix.
import "../contracts/InnoDEXOrderBook.sol";

contract InnoDEXOrderBookTest {
   

    InnoDEXOrderBook book;

    function checkInsertSellOrder () public {
        book = new InnoDEXOrderBook();
        bytes32 b99_1 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 99);
        Assert.equal(book.sellHead(), b99_1, "Sell top fail");
        
        bytes32 b99_2 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 99);
        Assert.equal(book.sellHead(), b99_1, "Sell top fail 2");
        Assert.equal(book.TEST_getSellNext(b99_1), b99_2, "Sell top fail 3");
        Assert.equal(book.TEST_getSellPrev(b99_2), b99_1, "Sell top fail 4");
        
        bytes32 b100 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 100);
        Assert.equal(book.TEST_getSellPrev(b100), b99_2, "Sell top fail 5");
        Assert.equal(book.TEST_getSellNext(b99_2), b100, "Sell top fail 6");
        
        bytes32 b98 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 98);
        Assert.equal(book.sellHead(), b98, "Sell top fail 7");
        Assert.equal(book.TEST_getSellPrev(b99_1), b98, "Sell top fail 8");
        Assert.equal(book.TEST_getSellNext(b98), b99_1, "Sell top fail 81");
        
        bytes32 b102 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 102);
        Assert.equal(book.TEST_getSellPrev(b102), b100, "Sell top fail 9");
        Assert.equal(book.TEST_getSellNext(b100), b102, "Sell top fail 10");
        
        bytes32 b101 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 101);
        Assert.equal(book.TEST_getSellPrev(b101), b100, "Sell top fail 9");
        Assert.equal(book.TEST_getSellNext(b101), b102, "Sell top fail 10");
        Assert.equal(book.TEST_getSellPrev(b102), b101, "Sell top fail 11");
        Assert.equal(book.TEST_getSellNext(b100), b101, "Sell top fail 12");
        
    }
    
    function checkInsertBuyOrder () public {
        book = new InnoDEXOrderBook();
        bytes32 b102_1 = book.insertOrder(InnoDEXOrderBook.OrderType.BUY, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 102);
        Assert.equal(book.buyHead(), b102_1, "Buy top fail");
        
        bytes32 b102_2 = book.insertOrder(InnoDEXOrderBook.OrderType.BUY, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 102);
        Assert.equal(book.buyHead(), b102_1, "Buy top fail 2");
        Assert.equal(book.TEST_getBuyNext(b102_1), b102_2, "Buy top fail 3");
        Assert.equal(book.TEST_getBuyPrev(b102_2), b102_1, "Buy top fail 4");
        
        bytes32 b101 = book.insertOrder(InnoDEXOrderBook.OrderType.BUY, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 101);
        Assert.equal(book.TEST_getBuyPrev(b101), b102_2, "Buy top fail 5");
        Assert.equal(book.TEST_getBuyNext(b102_2), b101, "Buy top fail 6");
        
        bytes32 b103 = book.insertOrder(InnoDEXOrderBook.OrderType.BUY, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 103);
        Assert.equal(book.buyHead(), b103, "Buy top fail 7");
        Assert.equal(book.TEST_getBuyPrev(b102_1), b103, "Buy top fail 8");
        Assert.equal(book.TEST_getBuyNext(b103), b102_1, "Buy top fail 81");
        
        bytes32 b99 = book.insertOrder(InnoDEXOrderBook.OrderType.BUY, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 99);
        Assert.equal(book.TEST_getBuyPrev(b99), b101, "Buy top fail 9");
        Assert.equal(book.TEST_getBuyNext(b101), b99, "Buy top fail 10");
        
        bytes32 b100 = book.insertOrder(InnoDEXOrderBook.OrderType.BUY, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 100);
        Assert.equal(book.TEST_getBuyPrev(b100), b101, "Buy top fail 9");
        Assert.equal(book.TEST_getBuyNext(b100), b99, "Buy top fail 10");
        Assert.equal(book.TEST_getBuyPrev(b99), b100, "Buy top fail 11");
        Assert.equal(book.TEST_getBuyNext(b101), b100, "Buy top fail 12");
        
    }
    
    // function checkCancelSellOrder() public {
    //     book = new InnoDEXOrderBook();
    //     bytes32 b98 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 98);
    //     bytes32 b99_1 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 99);
    //     bytes32 b99_2 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 99);
    //     bytes32 b100 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 100);
    //     bytes32 b101 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 101);
    //     bytes32 b102 = book.insertOrder(InnoDEXOrderBook.OrderType.SELL, InnoDEXOrderBook.OrderUrge.LIMIT, 1, 102);
        
    //     book.cancelSellOrder(b98);
    //     Assert.equal(book.sellHead(), b99_1, "Cancel sell fail");
    //     Assert.equal(book.TEST_getSellPrev(b99_1), "", "Cancel sell fail 1");
        
    //     book.cancelSellOrder(b99_2);
    //     Assert.equal(book.sellHead(), b99_1, "Cancel sell fail 3");
    //     Assert.equal(book.TEST_getSellNext(b99_1), b100, "Cancel sell fail 4");
    //     Assert.equal(book.TEST_getSellPrev(b100), b99_1, "Cancel sell fail 5");
        
    //     book.cancelSellOrder(b102);
    //     Assert.equal(book.TEST_getSellNext(b101), "", "Cancel sell fail 6");
    // }
    
}
