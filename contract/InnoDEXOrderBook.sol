// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.5;

contract InnoDEXOrderBook {
    event OrderAdded(bytes32 hash);
    
    enum OrderType { SELL, BUY }
    enum OrderUrge { LIMIT, MARKET }
    
    struct Order {
        bytes32 next;
        bytes32 prev;
        address owner;
        OrderType orderType;
        OrderUrge orderUrge;
        uint256 value;
        uint256 price;
    }

    bytes32 public sellHead;
    mapping (bytes32 => Order) public sellOrders;
    uint32 public sellLength = 0;
  
    bytes32 public buyHead;
    mapping (bytes32 => Order) public buyOrders;
    uint32 public buyLength = 0;
      
    uint32 public totalLength = 0;
    
    bool private matchingInProgress;
    address private contractOwner;
    
    constructor(address _contractOwner) {
        matchingInProgress = false;
        contractOwner = _contractOwner;
    }


    function sellTop() public view returns (bytes32, uint256, uint256) {
        return (sellHead, sellOrders[sellHead].value, sellOrders[sellHead].price);
    }
    
    function buyTop() public view returns (bytes32, uint256, uint256) {
        return (buyHead, buyOrders[buyHead].value, buyOrders[buyHead].price);
    }
    
    function removeBuyOrder(bytes32 orderID) public {
        require(msg.sender == contractOwner || msg.sender == address(this));
        bytes32 tmp_prev;
        bytes32 tmp_next;
        
        if (buyHead == orderID) {
            buyHead = buyOrders[orderID].next;
            buyOrders[buyHead].prev = "";
        } else {
            tmp_prev = buyOrders[orderID].prev;
            tmp_next = buyOrders[orderID].next;
            buyOrders[tmp_prev].next = tmp_next;
            buyOrders[tmp_next].prev = tmp_prev;
        }
        
        buyLength = buyLength - 1;
        totalLength = totalLength - 1;
    }
    
    function cancelBuyOrder(bytes32 orderID) public {
        require(msg.sender == buyOrders[orderID].owner);
        removeBuyOrder(orderID);
    }
    
    function removeSellOrder(bytes32 orderID) public {
        require(msg.sender == contractOwner || msg.sender == address(this));
        bytes32 tmp_prev;
        bytes32 tmp_next;
        
        if (sellHead == orderID) {
            sellHead = sellOrders[orderID].next;
            sellOrders[sellHead].prev = "";
        } else {
            tmp_prev = sellOrders[orderID].prev;
            tmp_next = sellOrders[orderID].next;
            sellOrders[tmp_prev].next = tmp_next;
            sellOrders[tmp_next].prev = tmp_prev;
        }
        
        sellLength = sellLength - 1;
        totalLength = totalLength - 1;
    }
    
    function cancelSellOrder(bytes32 orderID) public {
        require(msg.sender == sellOrders[orderID].owner);
        removeSellOrder(orderID);
    }
    
    // function TEST_getSellNext(bytes32 hash) public view returns (bytes32) {
    //     return sellOrders[hash].next;
    // }
    // function TEST_getSellPrev(bytes32 hash) public view returns (bytes32) {
    //     return sellOrders[hash].prev;
    // }
    
    // function TEST_getBuyNext(bytes32 hash) public view returns (bytes32) {
    //     return buyOrders[hash].next;
    // }
    // function TEST_getBuyPrev(bytes32 hash) public view returns (bytes32) {
    //     return buyOrders[hash].prev;
    // }

    
    function getOrderPrice(bytes32 hash) public view returns (uint256) {
        if (sellOrders[hash].owner != address(0))
            return sellOrders[hash].price;
        else if (buyOrders[hash].owner != address(0))
            return buyOrders[hash].price;
        else
            return 0;
    }
    
    function getOrderValue(bytes32 hash) public view returns (uint256) {
        if (sellOrders[hash].owner != address(0))
            return sellOrders[hash].value;
        else if (buyOrders[hash].owner != address(0))
            return buyOrders[hash].value;
        else
            return 0;
    }
    
    function setOrderValue(bytes32 hash, uint256 newValue) public {
        require(msg.sender == contractOwner);
        if (sellOrders[hash].owner != address(0))
            sellOrders[hash].value = newValue;
        else if (buyOrders[hash].owner != address(0))
            buyOrders[hash].value = newValue;
    }
    
    function getOrderOwner(bytes32 hash) public returns (address) {
        if (sellOrders[hash].owner != address(0))
            return sellOrders[hash].owner;
        else if (buyOrders[hash].owner != address(0))
            return buyOrders[hash].owner;
        else
            return address(0);
    }
    
    function insertOrder(OrderType _orderType, OrderUrge _orderUrge, address owner, uint256 _value, uint256 _price) public returns (bytes32) {
        require(msg.sender == contractOwner);
        Order memory order = Order("", "", owner, _orderType, _orderUrge, _value, _price);
        bytes32 newID = keccak256(abi.encodePacked(order.owner, order.orderType, order.orderUrge, order.price, order.value, block.timestamp, totalLength));
        assert(newID != "");
        bytes32 ptr;
        bytes32 tmp_next;
        bytes32 tmp_prev;
        
        totalLength = totalLength + 1;
        
        if (_orderType == OrderType.SELL) {
            sellLength = sellLength + 1;
            sellOrders[newID] = order;
            
            if (sellHead == "") {
                // Insert in empty
                sellHead = newID;
                emit OrderAdded(newID);
                return newID;
            }
            
            if (sellOrders[sellHead].price > _price) {
                // Insert instead of head
                sellOrders[sellHead].prev = newID;
                sellOrders[newID].next = sellHead;
                sellHead = newID;
                emit OrderAdded(newID);
                return newID;
            }
            
            ptr = sellHead;
            
            while (_price >= sellOrders[ptr].price && sellOrders[ptr].next != "") {
                ptr = sellOrders[ptr].next;
            }
            assert(ptr != "");
            
            if (_price >= sellOrders[ptr].price && sellOrders[ptr].next == "") {
                // The found node is last, and the new item belongs to the end
                assert(ptr != "");
                sellOrders[ptr].next = newID;
                sellOrders[newID].prev = ptr;
            } else {
                // Insert before found node
                assert(sellOrders[ptr].prev != "");
                tmp_prev = sellOrders[ptr].prev;
                sellOrders[newID].prev = tmp_prev;
                sellOrders[newID].next = ptr;
                sellOrders[tmp_prev].next = newID;
                sellOrders[ptr].prev = newID;
            }
        } else {
            buyLength = buyLength + 1;
            buyOrders[newID] = order;
            
            if (buyHead == "") {
                // Insert in empty
                buyHead = newID;
                emit OrderAdded(newID);
                return newID;
            }
            
            if (buyOrders[buyHead].price < _price) {
                // Insert instead of head
                buyOrders[buyHead].prev = newID;
                buyOrders[newID].next = buyHead;
                buyHead = newID;
                emit OrderAdded(newID);
                return newID;
            }
            
            ptr = buyHead;
            
            while (_price <= buyOrders[ptr].price && buyOrders[ptr].next != "") {
                ptr = buyOrders[ptr].next;
            }
            assert(ptr != "");
            
            if (_price <= buyOrders[ptr].price && buyOrders[ptr].next == "") {
                // The found node is last, and the new item belongs to the end
                assert(ptr != "");
                buyOrders[ptr].next = newID;
                buyOrders[newID].prev = ptr;
            } else {
                // Insert before found node
                assert(buyOrders[ptr].prev != "");
                tmp_prev = buyOrders[ptr].prev;
                buyOrders[newID].prev = tmp_prev;
                buyOrders[newID].next = ptr;
                buyOrders[tmp_prev].next = newID;
                buyOrders[ptr].prev = newID;
            }
        }
        
        emit OrderAdded(newID);
        return newID;
    }

}