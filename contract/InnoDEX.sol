// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.5;
import "InnoDEXOrderBook.sol";

contract ERC20 {
    string public name;
    string public symbol;
    uint8 public decimals;  
    
    address public owner;
    string public website;
    ERC20 public baseAddress;
    
    InnoDEXOrderBook orderBook;
    bool private matchingInProgress;

    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
    event Transfer(address indexed from, address indexed to, uint tokens);

    mapping(address => uint256) public balances;
    mapping(address => uint256) private frozen;

    mapping(address => mapping (address => uint256)) allowed;
    
    uint256 totalSupply_;

    using SafeMath for uint256;

    constructor(uint256 total, string memory _name, string memory _symbol, uint8 _decimals, address _owner, string memory _website) public {  
    	totalSupply_ = total;
    	balances[_owner] = totalSupply_;
    	name =  _name;
    	symbol = _symbol;
    	decimals = _decimals;
    	owner = _owner;
    	website = _website;
    	baseAddress = this;
    	orderBook = new InnoDEXOrderBook(address(this));
    }
    
    function contractBalance() public view returns (uint256) {
        return address(this).balance;
    }
    
    function usableBalance(address user) public view returns (uint256) {
        return balances[user].sub(frozen[user]);
    }

    function totalSupply() public view returns (uint256) {
	    return totalSupply_;
    }
    
    function balanceOf(address tokenOwner) public view returns (uint) {
        return balances[tokenOwner];
    }

    function transfer(address ownerIn,address receiver, uint numTokens) private {
        require(numTokens <= balances[ownerIn]);
        balances[ownerIn] = balances[ownerIn].sub(numTokens);
        balances[receiver] = balances[receiver].add(numTokens);
        emit Transfer(ownerIn, receiver, numTokens);
    }

    function approve(address delegate, uint numTokens) public {
        allowed[msg.sender][delegate] = numTokens;
        emit Approval(msg.sender, delegate, numTokens);
    }

    function allowance(address _owner, address delegate) public view returns (uint) {
        return allowed[_owner][delegate];
    }
    
    function freezeTokens(address _owner, uint256 amount) private {
        require(usableBalance(_owner) >= amount);
        frozen[_owner] = frozen[_owner].add(amount);
    }
    
    function unfreezeTokens(address _owner, uint256 amount) private {
        require(frozen[owner] >= amount);
        frozen[_owner] = frozen[_owner].sub(amount);
    }

    function transferFrom(address _owner, address buyer, uint numTokens) public {
        require(numTokens <= balances[_owner]);    
        require(numTokens <= allowed[_owner][msg.sender]);
    
        balances[_owner] = balances[_owner].sub(numTokens);
        allowed[_owner][msg.sender] = allowed[_owner][msg.sender].sub(numTokens);
        balances[buyer] = balances[buyer].add(numTokens);
        emit Transfer(_owner, buyer, numTokens);
    }
    
    function placeOrder(InnoDEXOrderBook.OrderType orderType, InnoDEXOrderBook.OrderUrge orderUrge, uint32 value, uint32 price) public payable returns (bytes32) {
        require(value > 0);
        require(price > 0);
        // require(value * price > 10000);
        if (orderType == InnoDEXOrderBook.OrderType.SELL) {
            require(usableBalance(msg.sender) >= value);
            freezeTokens(msg.sender, value);
        } else if (orderType == InnoDEXOrderBook.OrderType.BUY) {
            require(msg.value >= value * price);
        }
        
        bytes32 tmp = orderBook.insertOrder(orderType, orderUrge, msg.sender, value, price);
        matching();
        return tmp;
    }
    
    function cancelOrder(InnoDEXOrderBook.OrderType orderType, bytes32 orderID) public {
        
        if (orderType == InnoDEXOrderBook.OrderType.SELL) {
            unfreezeTokens(msg.sender, orderBook.getOrderValue(orderID));
            orderBook.cancelSellOrder(orderID);
        }
        else if (orderType == InnoDEXOrderBook.OrderType.BUY) {
            uint256 orderPrice = orderBook.getOrderPrice(orderID);
            uint256 orderValue = orderBook.getOrderValue(orderID);
            payable(msg.sender).transfer(orderPrice * orderValue);
            orderBook.cancelBuyOrder(orderID);
        }
    }
    
    function orderBookSellTop() public view returns (bytes32, uint256, uint256) {
        return orderBook.sellTop();
    }
    
    function orderBookBuyTop() public view returns (bytes32, uint256, uint256) {
        return orderBook.buyTop();
    }
    
    
    function matching() private {
        if (matchingInProgress) // Other thread is active
            return;
            
        bytes32 sellHead = orderBook.sellHead();
        bytes32 buyHead = orderBook.buyHead();
        uint256 topBuyPrice;
        uint256 topSellPrice;
        uint256 topBuyValue;
        uint256 topSellValue;
        address sellOwner;
        address buyOwner;
        
        matchingInProgress = true;    
        while (buyHead != "" && sellHead != "") {
            topBuyPrice = orderBook.getOrderPrice(buyHead);
            topSellPrice = orderBook.getOrderPrice(sellHead);
            topBuyValue = orderBook.getOrderValue(buyHead);
            topSellValue = orderBook.getOrderValue(sellHead);
            sellOwner = orderBook.getOrderOwner(sellHead);
            buyOwner = orderBook.getOrderOwner(buyHead);
            
            if (topBuyPrice >= topSellPrice) {
                if (topBuyValue < topSellValue) {
                    // Buy order satisfied fully
                    orderBook.setOrderValue(sellHead, topSellValue.sub(topBuyValue));
                    orderBook.setOrderValue(buyHead, 0);
                    orderBook.removeBuyOrder(buyHead);
                } else if (topBuyValue > topSellValue) {
                    // Sell order satisfied fully
                    orderBook.setOrderValue(buyHead, topBuyValue.sub(topSellValue));
                    orderBook.setOrderValue(sellHead, 0);
                    orderBook.removeSellOrder(sellHead);
                } else {
                    orderBook.setOrderValue(buyHead, 0);
                    orderBook.setOrderValue(sellHead, 0);
                    orderBook.removeBuyOrder(buyHead);
                    orderBook.removeSellOrder(sellHead);
                }
                
                payable(sellOwner).transfer(topBuyValue * topBuyPrice); 
                transfer(sellOwner, buyOwner, topBuyValue); 
                unfreezeTokens(sellOwner, topBuyValue);
            } else {
                // No orders can be matched.
                break;
            }
            sellHead = orderBook.sellHead();
            buyHead = orderBook.buyHead();
        }
        matchingInProgress = false;
    }
}

library SafeMath { 
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
      assert(b <= a);
      return a - b;
    }
    
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
      uint256 c = a + b;
      assert(c >= a);
      return c;
    }
}

contract contractsHolder {
    mapping(address => ERC20) public adresess;
    mapping(address => uint256) public supplies;
    ERC20 public calledERC20;
    
    function createToken (uint256 total, string memory name, string memory symbol, uint8 decimals, string memory website) public returns (ERC20) {
        ERC20 c = new ERC20(total, name, symbol, decimals, msg.sender, website);
        adresess[msg.sender] = c;
        return c;
    }
    
    function swapOwners (address _with) public { /* payable {
        require(msg.value == 5 * 10 ** 17, "Must pay 0.5 ether to do this"); */
        
        ERC20 tmp = adresess[msg.sender];
        tmp.totalSupply();
        adresess[msg.sender] = adresess[_with];
        adresess[_with] = tmp;
    }
    
    // function returnTotal() view public returns (uint256) {
    //     ERC20 tmp = ERC20(adresess[msg.sender]);
    //     return tmp.totalSupply();
    // }
    
    // function returnowner() view public returns (address) {
    //     ERC20 tmp = ERC20(adresess[msg.sender]);
    //     return tmp.owner();
    // }
    
    // function SendToken(address receiver, uint numTokens) public /*payable*/ {
    //     ERC20 tmp = ERC20(adresess[msg.sender]);
    //     // adresess[msg.sender].call(bytes4(keccak256("transfer(uint256)")), receiver,numTokens);
    //     tmp.transfer(msg.sender,receiver,numTokens);
    // }
    
    function balanceOf(address tokenOwner) public view returns (uint) {
        ERC20 tmp = ERC20(adresess[msg.sender]);
        return tmp.balances(tokenOwner);
    }
    
    
}