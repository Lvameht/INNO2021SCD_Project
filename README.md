# INNO2021SCD_Project
by Muhammad Mavlyutov

## Assumptions and requirements
- There is a holder contract already deployed
- There is no concurrency between users
- Token contract holders share its address for other people to participate in exchange

## Architecture
This implementation is ERC20 token contract with included orderbook. Each token contract have separate orderbook.

Tokens must be created via ContractHolder contract.

Orders in orderbook are stored in linked list. Matching engine fires up on every order insertion.

Token buyers must deposit the full price to the ERC20 contract. This deposit will be tranferred to the seller, if orders are matched, or returned back to the buyer, in case of cancelling the order.
